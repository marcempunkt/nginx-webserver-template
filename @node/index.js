const express = require("express");
const app = express();

/* Enable CORS only in dev environment */
// if (process.env.NODE_ENV === "dev") {
//   console.warn(colors.yellow("CORS Middleware is enabled."));
//   app.use(cors());
// }

app.get("/", (req, res) => {
  res.send("Hello World");
});

const PORT = 4000;
app.listen(PORT, () => console.log(`Server listening on Port ${PORT}`));
