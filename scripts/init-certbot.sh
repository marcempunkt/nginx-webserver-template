#!/bin/bash
#### --------------------------------------------- Check if docker-compose is installed
## -x                         True if file exists and is executable
## command -v COMMAND         will look up and return the PATH of the COMMAND
if ! [ -x "$(command -v docker-compose)" ]; then
  echo 'Error: docker-compose is not installed.' >&2
  exit 1
fi

#### --------------------------------------------- Variables
domains_reactmaeurerdev=(react.maeurer.dev www.react.maeurer.dev)
domains_nodemaeurerdev=(node.maeurer.dev www.node.maeurer.dev)
rsa_key_size=4096
data_path="./docker/data/certbot"
email="" # Adding a valid address is strongly recommended
staging=0 # Set to 1 if you're testing your setup to avoid hitting request limits

#### --------------------------------------------- Check if there are already certs
## -d         True if file exists and is a directy
if [ -d "$data_path" ]; then
  read -p "Existing data found for $domains. Continue and replace existing certificate? (y/N) " decision
  if [ "$decision" != "Y" ] && [ "$decision" != "y" ]; then
    exit
  fi
fi

#### --------------------------------------------- Download options-ssl-nginx.conf & ssl-dhparams.pem
if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
  echo "### Downloading recommended TLS parameters ..."
  mkdir -p "$data_path/conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "$data_path/conf/options-ssl-nginx.conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "$data_path/conf/ssl-dhparams.pem"
  echo
fi

#### --------------------------------------------- Create dummy certificates 
## so that nginx will start with ssl without an error
## ----------------- react.maeurer.dev
echo "### Creating dummy certificate for $domains_reactmaeurerdev ..."
path_reactmaeurerdev="/etc/letsencrypt/live/$domains_reactmaeurerdev"
mkdir -p "$data_path/conf/live/$domains_reactmaeurerdev"
docker-compose -f ./docker/docker-compose.yaml run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path_reactmaeurerdev/privkey.pem' \
    -out '$path_reactmaeurerdev/fullchain.pem' \
    -subj '/CN=localhost'" certbot
echo
## ----------------- node.maeurer.dev
echo "### Creating dummy certificate for $domains_nodemaeurerdev ..."
path_nodemaeurerdev="/etc/letsencrypt/live/$domains_nodemaeurerdev"
mkdir -p "$data_path/conf/live/$domains_nodemaeurerdev"
docker-compose -f ./docker/docker-compose.yaml run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path_nodemaeurerdev/privkey.pem' \
    -out '$path_nodemaeurerdev/fullchain.pem' \
    -subj '/CN=localhost'" certbot
echo

#### --------------------------------------------- Start nginx-master container
echo "### Starting nginx ..."
## --force-recreate              Recreate containers even if their configuration and image haven't been changed
## -d                            detach the container: Run in the background
docker-compose -f ./docker/docker-compose.yaml up --force-recreate -d nginx-master
echo

#### --------------------------------------------- Delete dummy certs
echo "### Deleting dummy certificate ..."
## Go inside the certbot container and remove all the dummy certificates
## docker-compose run             detach the container: Run in the background
## --rm                           automatically remove the container when it exists
## --entrypoint                   ovveride the entrypoint of the image
docker-compose -f ./docker/docker-compose.yaml run --rm --entrypoint "\
  rm -Rf /etc/letsencrypt/live/$domains_reactmaeurerdev && \
  rm -Rf /etc/letsencrypt/archive/$domains_reactmaeurerdev && \
  rm -Rf /etc/letsencrypt/renewal/$domains_reactmaeurerdev.conf \
  rm -Rf /etc/letsencrypt/live/$domains_nodemaeurerdev && \
  rm -Rf /etc/letsencrypt/archive/$domains_nodemaeurerdev && \
  rm -Rf /etc/letsencrypt/renewal/$domains_nodemaeurerdev.conf \ " certbot
echo

#### --------------------------------------------- Create let's encrypt certs
# Select appropriate email arg
case "$email" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $email" ;;
esac

# Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

## ----------------- react.maeurer.dev
echo "### Requesting Let's Encrypt certificate for $domains_reactmaeurerdev ..."
domain_reactmaeurerdev_args=""
for domain in "${domains_reactmaeurerdev[@]}"; do
  domain_reactmaeurerdev_args="$domain_reactmaeurerdev_args -d $domain"
done

## Create valid ssl certs with certbot
docker-compose -f ./docker/docker-compose.yaml run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/ \
    $staging_arg \
    $email_arg \
    $domain_reactmaeurerdev_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" certbot
echo

## ----------------- node.maeurer.dev
echo "### Requesting Let's Encrypt certificate for $domains_nodemaeurerdev ..."
domain_nodemaeurerdev_args=""
for domain in "${domains_nodemaeurerdev[@]}"; do
  domain_nodemaeurerdev_args="$domain_nodemaeurerdev_args -d $domain"
done

docker-compose -f ./docker/docker-compose.yaml run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/ \
    $staging_arg \
    $email_arg \
    $domain_nodemaeurerdev_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" certbot
echo

#### --------------------------------------------- Reload nginx-master with the let's encrypt SSL Certificates
echo "### Reloading nginx ..."
docker-compose -f ./docker/docker-compose.yaml exec nginx-master nginx -s reload
